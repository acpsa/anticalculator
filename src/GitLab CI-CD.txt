
----------------------------------------------------------------- PART I -----------------------------------------------------------------
GITLAB RUNNER SETUP				<https://www.youtube.com/watch?v=2MBhxk2chhM>
	
1. Install Gitlab Runner
2. Register Runner
	C:\Command Line Tools>gitlab-runner.exe register
	Runtime platform                                    arch=amd64 os=windows pid=38748 revision=b37d3da9 version=14.3.0
	Enter the GitLab instance URL (for example, https://gitlab.com/):
	https://gitlab.com/
	Enter the registration token:
	PSSh1Uf2dkk-Yyvcoofo
	Enter a description for the runner:
	[BDC6-LX-G01FPJ4]: ACP_ACN_Runner1
	Enter tags for the runner (comma-separated):
	ssh, ci
	Registering runner... succeeded                     runner=PSSh1Uf2
	Enter an executor: shell, ssh, virtualbox, docker+machine, custom, docker, docker-windows, parallels, kubernetes, docker-ssh, docker-ssh+machine:
	shell
	Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
3. Install Runner as service 	
	C:\Command Line Tools>gitlab-runner.exe install
	Runtime platform                                    arch=amd64 os=windows pid=23248 revision=b37d3da9 version=14.3.0
4. Start Runner Service
	C:\Command Line Tools>gitlab-runner.exe start
	Runtime platform                                    arch=amd64 os=windows pid=25204 revision=b37d3da9 version=14.3.0
5. Verify Runner is Running
	a.	Goto Gitlab PRoject Repo
	b.	GoTo Settings
	c.	Goto CI/CD
	d.	Expand Runners and you can see in the botton Left that Runner is available.
		IT Should be Green (Running)


----------------------------------------------------------------- PART II ----------------------------------------------------------------

GITLAB CI/CD SETUP					<https://www.youtube.com/watch?v=jUiKi6FWYrg>
	

