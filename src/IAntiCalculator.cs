﻿namespace Acpsa.Anticalculator
{
    public interface IAntiCalculator
    {
        public double Subtract(double first, double second);        
        public double Subtract(int first, int second);
        public double Sum(double first, double second);
        //public double Sum(int first, int second);

    }
}
